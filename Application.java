package com.attra;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// programmer p=new Programmer();
		//BeanFactory factory = new XmlBeanfactory( new FileSystemResource("my-spring-beans.xml"));
            ApplicationContext context= new ClassPathXmlApplicationContext("my-spring-beans.xml");
		 Programmer programmer=(Programmer) context.getBean("programmer");
		programmer.code();

		// p.code
	}

}
