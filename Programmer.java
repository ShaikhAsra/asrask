package com.attra;

public class Programmer {
	
	private String name;
	private int Salary;

	
	public int getSalary() {
		return Salary;
	}
	public Programmer(String name, int Salary) {
		this.name=name;
		this.Salary=Salary;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void code() {
	
		System.out.println(getName()+" got Salary of"+getSalary()+"Lacs");

	}

}
